const staticCalendar = "e-calendar-v1"
const assets = [
  "/",
  "/login",
  "/register",
  "/dashboard",
  "/departments",
  "/users",
  "/css/app.css",
  "/fullcalendar/lib/main.min.css",
  "/fullcalendar/lib/main.js",
  "/js/app.js",
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticCalendar).then(cache => {
      cache.addAll(assets)
    })
  )
})

self.addEventListener("fetch", fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then(res => {
      return res || fetch(fetchEvent.request)
    })
  )
})