(async () => {

    document.addEventListener('DOMContentLoaded', function() {

        jQuery.ajax({
            url: '/notifs',
            type: 'GET',
            dataType: 'json',
            data: {},
        }).always(function(e) {
            console.log(e);
            if (e.length > 0) {
                for (var i = 0; i < e.length; i++) {
                    
                    // var msg = 'Upcoming Event: '+e[i].name+' Start: '+e[i].start_date+' End: '+e[i].end_date+' info:'+e[i].description;

                    showNotif(e[i].name,e[i].description,'/events/'+e[i].id);
                }
            }
        });
    });   

    function showNotif(title,msg,url){

        // create and show the notification
        const showNotification = () => {

            // create a new notification
            const notification = new Notification(title, {
                body: msg,
                icon: '/images/icons/icon-96x96.png'
            });

            // close the notification after 10 seconds
            setTimeout(() => {
                notification.close();
            }, 10 * 1000);

            // navigate to a URL when clicked
            notification.addEventListener('click', () => {
                window.open(url, '_blank');
            });

        }

        // show an error message
        const showError = () => {
            console.log('You blocked the notifications');
        }

        // check notification permission
        let granted = false;

        if (Notification.permission === 'granted') {
            granted = true;
        } else if (Notification.permission !== 'denied') {
            let permission = Notification.requestPermission();
            granted = permission === 'granted' ? true : false;
        }

        // show notification or error
        granted ? showNotification() : showError();
    }

})();
