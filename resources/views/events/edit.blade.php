@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Edit Event') }}</span>
                </div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <?php 
                    $departmentids = json_decode($event->department_ids); 
                    $departmentids = is_array($departmentids) ? $departmentids:[];
                    ?>
                    <form action="{{ route('events.update',$event->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>Event Name:</strong>
                                    <input type="text" name="name" value="{{ $event->name }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <strong>Venue:</strong>
                                    <input type="text" name="venue" value="{{ $event->venue }}" class="form-control" placeholder="">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>Guests:</strong>
                                            <select name="guests" class="form-control">
                                                <option value="all"{{ $event->guests == 'all' ? ' selected':'' }}>All</option>
                                                <option value="student"{{ $event->guests == 'student' ? ' selected':'' }}>Students only</option>
                                                <option value="faculty"{{ $event->guests == 'faculty' ? ' selected':'' }}>Faculty only</option>
                                                <option value="staff"{{ $event->guests == 'staff' ? ' selected':'' }}>Staff only</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>Department:</strong><br>
                                            @foreach($departments as $department)
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" name="department_ids[]" type="checkbox" id="dep-{{  $department->id }}" value="{{  $department->id }}"{{ in_array($department->id, $departmentids) ? ' checked':'' }}>
                                                    <label class="form-check-label" for="dep-{{  $department->id }}">{{ $department->name }}</label>
                                                </div><br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">

                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <strong>Start Date:</strong>
                                                    <input type="date" name="start_date" value="{{ date('Y-m-d', strtotime($event->start_date)) }}" class="form-control" placeholder="MM/DD/YYYY">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <strong>Start Time:</strong>
                                                    <input type="time" name="start_time" value="{{ date('H:i:s', strtotime($event->start_date)) }}" class="form-control" placeholder="h:m:s a">
                                                </div>  
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <strong>End Date:</strong>
                                                    <input type="date" name="end_date" value="{{ date('Y-m-d', strtotime($event->end_date)) }}" class="form-control" placeholder="MM/DD/YYYY">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <strong>End Time:</strong>
                                                    <input type="time" name="end_time" value="{{ date('H:i:s', strtotime($event->end_date)) }}" class="form-control" placeholder="h:m:s a">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <strong>Description:</strong>
                                    <textarea rows="5" name="description" class="form-control">{{ $event->description }}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary">Update Event</button>
                            </div>
                        </div>
                    </form>  

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
