@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Departments') }}</span>
                    <a href="{{ route('departments.create') }}" class="btn btn-primary float-right btn-sm">Add Department</a>
                </div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if( $departments->count() )
                    <table class="table table-inverse table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($departments as $dep)
                            <tr>
                                <td>{{ $dep->name }}</td>
                                <td>
                                    <a href="{{ route('departments.edit',$dep->id) }}" class="btn btn-success btn-sm" style="">Edit</a>
                                    <form action="{{ route('departments.destroy',$dep->id) }}" method="POST" style="float:right;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</a>
                                    </form>
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $departments->render() !!}
                    @else
                        <div class="alert alert-info" role="alert">
                            There are no avaialble departments to show at the moment.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
