@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Edit Department') }}</span>
                </div>

                <div class="card-body">
                    
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    <form action="{{ route('departments.update',$department->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>Department Name:</strong>
                                    <input type="text" name="name" value="{{ $department->name }}" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary">Update Department</button>
                            </div>
                        </div>
                    </form>  

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
