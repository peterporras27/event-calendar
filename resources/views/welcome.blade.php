@extends('layouts.app')

@section('content')
<div class="container">
    <div align="center" class="mb-3">
        <a href="{{ asset('wvsucalendar.apk') }}" class="btn btn-lg btn-success">
            Download Android App
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id='calendar'></div>
        </div>
    </div>

</div>
@endsection
@section('styles')
<link href='{{asset('fullcalendar/lib/main.min.css')}}' rel='stylesheet' />
<style>
.fc-event-title {font-size: 18px;}
@media (max-width: 767px) {.fc .fc-toolbar-title{font-size: 1.3em;} #app {padding-top: 0;}}
</style>
@endsection
@section('scripts')
<script src='{{asset('fullcalendar/lib/main.js')}}'></script>
<script>
window.mobilecheck = function() {
    return (window.innerWidth < 768);
};

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    //console.log(jQuery('#calendar').fullCalendar('getDate'));
    var calendar = new FullCalendar.Calendar(calendarEl, {
        //initialView: 'listWeek',
        initialView: window.mobilecheck() ? "listWeek" : "dayGridMonth",
        height: window.mobilecheck() ? 500: "auto",
        // initialView: 'dayGridMonth',
        events: {
            url: '{{ route('dates') }}',
            type: 'GET',
            data: {month: ''},
            error: function(e) {
              // alert('there was an error while fetching events!');
              console.log(e);
            },
            // color: 'yellow',   // a non-ajax option
            // textColor: 'black' // a non-ajax option
        },
        eventClick: function(info) {

            console.log(info);
            
            // info.el.style.borderColor = 'red';
        }

    });
    calendar.render();
});

</script>
@endsection
