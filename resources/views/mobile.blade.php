@extends('layouts.app')

@section('content')

<div class="container">

    <div id="boxes">

        @if($results && $results->count())

            <table class="table table-bordered mt-5 mb-5">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $result)
                    <tr>
                        <td>
                            <a href="{{ route('event',$result->id) }}">
                                {{ $result->name }}
                            </a>
                        </td>
                        <td>
                            {{ Carbon\Carbon::parse($result->start_date)->format('F j, Y, g:i a') }} - {{ Carbon\Carbon::parse($result->end_date)->format('F j, Y, g:i a') }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        @else

            <form action="{{ route('mobile') }}" method="GET">
                <div class="input-group mt-5 mb-4">
                    <input type="text" name="s" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col">
                    <div class="card">
                      <div class="card-body" align="center">
                        <a class="nav-link" onclick="swap('notes')" href="#">
                            <img src="{{ asset('images/notes.png') }}" alt="" class="img-fluid">
                            <br>Notes
                        </a>
                      </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                      <div class="card-body" align="center">
                        <a class="nav-link" onclick="swap('calendar')" href="#">
                            <img src="{{ asset('images/calendar.png') }}" alt="" class="img-fluid">
                            <br>Events
                        </a>
                      </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-6 mt-3">
                    <div class="card">
                      <div class="card-body" align="center">
                        <a class="nav-link active" onclick="swap('holidays')" href="#">
                            <img src="{{ asset('images/holiday.png') }}" alt="" class="img-fluid">
                            <br>Holidays
                        </a>
                      </div>
                    </div>
                </div>
            </div>

        @endif

    </div>

    <div id="content-calendar" style="display:none;" class="content mt-5">
        <div id='calendar'></div>
    </div>

    <div id="content-holidays" style="display:none;" class="content mt-5">
        <div id='holidays'>
            
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td><h5>New Year's Day</h5>Jan 1, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Lunar New Year</h5>Feb 12, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Maundy Thursday</h5>Apr 1, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Good Friday</h5>Apr 2, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Bataan Day</h5>Apr 9, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Labour Day</h5>May 1, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Eid al-Fitr</h5>May 13, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Philippines Independence Day</h5>Jun 12, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Eid al-Adha</h5>Jul 20, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>National Heroes' Day (in Philippines)</h5>Aug 30, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Bonifacio Day</h5>Nov 30, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Feast of the Immaculate Conception</h5>Dec 8, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Christmas Day</h5>Dec 25, {{ date('Y') }}</td>
                    </tr>
                    <tr>
                        <td><h5>Rizal Day</h5>Dec 30, {{ date('Y') }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="content-notes" style="display:none;" class="content mt-5">
        <div class="notes-form" style="display:none;">
            <input type="text" class="form-control" placeholder="Title">
            <textarea style="width:100%;" rows="3" placeholder="Notes..." class="form-control"></textarea>
            <button onclick="addNote()" class="btn btn-success btn-block">Add Note</button>
        </div>

        <div class="accordion" id="notes">
        </div>

    </div>


</div>
<div class="bottom-nav" style="display: none;">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body" align="center">
                    <a onclick="swap('holidays')" href="#">
                        <img src="{{ asset('images/holiday.png') }}" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body" align="center">
                    <a onclick="swap('calendar')" href="#">
                        <img src="{{ asset('images/calendar.png') }}" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body" align="center">
                    <a onclick="swap('notes')" href="#">
                        <img src="{{ asset('images/notes.png') }}" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('styles')
<link rel="stylesheet" href="{{asset('css/main.css')}}">
<link href='{{asset('fullcalendar/lib/main.min.css')}}' rel='stylesheet' />
<style>
.navbar-toggler{display: none;}
.fc-event-title {font-size: 18px;}
.card .nav-link {color:  #000;}
.bottom-nav{
    position: fixed;
    bottom: 0;
    width: 100%;
    padding: 10px 30px;
    background: #fff;
}
#holidays td {width: 100%;}
#holidays tr {width: 100%;}
.navbar {position: fixed !important;top: 0;width: 100%;}
.content, #boxes {margin: 30px 0 100px 0;}
.notes-form{margin-bottom: 15px;}
.notes-form input, .notes-form textarea{margin-bottom: 5px;}
#notes .btn-danger{position: absolute;top: 10px;right: 10px;}
@media (max-width: 767px) {.fc .fc-toolbar-title{font-size: 1.3em;} #app {padding-top: 0;}}
</style>
@endsection
@section('scripts')
{{-- <script src="{{asset('js/main.js')}}" type="module"></script> --}}
<script src='{{asset('fullcalendar/lib/main.js')}}'></script>
<script>
window.mobilecheck = function() {
    return (window.innerWidth < 768);
};

function swap(target)
{
    jQuery('.content').hide();
    jQuery('#content-'+target).fadeIn();
    jQuery('#boxes').hide();
    jQuery('.bottom-nav').fadeIn();

    switch(target){
        case 'holidays': 
            jQuery('.navbar-brand').html('Holidays'); 
            jQuery('.add').remove(); 
        break;
        case 'calendar': 
            jQuery('.navbar-brand').html('Events'); 
            jQuery('.add').remove(); 
        break;
        case 'notes': 
            jQuery('.navbar-brand').html('Notes'); 
            jQuery('.add').remove(); 
            jQuery('.navbar-toggler').after('<a href="#" onclick="newNote()" class="add btn btn-success btn-sm"><b>&#43;</b></a>');
        break;
    }
}

function newNote(){
    jQuery('.notes-form').fadeIn();
}

function addNote(){
    var title = jQuery('.notes-form input').val();
    var content = jQuery('.notes-form textarea').val();

    if (title == '' ) { return; }
    if (content == '' ) { return; }

    var data = JSON.stringify({title:title,content:content});
    var slug = slugMe(title);
    var slugs = localStorage.getItem('slugs');
    
    if (slugs) {
        
        slugs = JSON.parse(slugs);

    } else {

        slugs = [];
    }

    if (!slugs.includes(slug)) {
        slugs.push(slug);
    }

    localStorage.setItem(slug, data);
    localStorage.setItem('slugs', JSON.stringify(slugs));

    jQuery('.notes-form').fadeOut();
    loadNotes();

    jQuery('.notes-form input').val('');
    jQuery('.notes-form textarea').html('');
}

function loadNotes(){
    var slugs = localStorage.getItem('slugs');
    if (slugs) {    

        slugs = JSON.parse(slugs);
        if (slugs.length > 0) {
            jQuery('#notes').html('');
            for (var i = slugs.length - 1; i >= 0; i--) {
                var item = JSON.parse(localStorage.getItem(slugs[i]));
                var html = '<div class="card"><div class="card-header" id="'+slugMe(item.title)+'">';
                html += '<h2 class="mb-0"><button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#'+slugMe(item.title)+'-collapse" aria-expanded="true" aria-controls="'+slugMe(item.title)+'-collapse">';
                html += item.title;
                html += '</button>';
                html += '<a href="#" onclick="deleteNote(\''+slugMe(item.title)+'\')" class="btn btn-sm btn-danger">&#x2716;</a>';
                html += '</h2></div>';
                html += '<div id="'+slugMe(item.title)+'-collapse" class="collapse" aria-labelledby="'+slugMe(item.title)+'" data-parent="#notes">';
                html += '<div class="card-body">'+item.content+'</div></div></div>';

                jQuery('#notes').append(html);
            }
        }
    }
}

function deleteNote(slug){
    localStorage.removeItem(slug);
    var slugs = localStorage.getItem('slugs');
    if (slugs) {
        slugs = JSON.parse(slugs);
        var index = slugs.indexOf(slug);
        if (index > -1) {slugs.splice(index, 1);}
        localStorage.setItem('slugs', JSON.stringify(slugs));
        jQuery('#'+slug).parent().remove();
    }
}

function slugMe(Text) {
  return Text.toLowerCase()
             .replace(/ /g, '-')
             .replace(/[^\w-]+/g, '');
}

document.addEventListener('DOMContentLoaded', function() {

    if (window.mobilecheck()) {
        jQuery('.navbar-brand').attr('href','/mobile');
    }

    var calendarEl = document.getElementById('calendar');
    //console.log(jQuery('#calendar').fullCalendar('getDate'));
    var calendar = new FullCalendar.Calendar(calendarEl, {
        //initialView: 'listWeek',
        initialView: window.mobilecheck() ? "listWeek" : "dayGridMonth",
        height: window.mobilecheck() ? 500: "auto",
        // initialView: 'dayGridMonth',
        events: {
            url: '{{ route('dates') }}',
            type: 'GET',
            data: {month: ''},
            error: function(e) {
              // alert('there was an error while fetching events!');
              console.log(e);
            },
            // color: 'yellow',   // a non-ajax option
            // textColor: 'black' // a non-ajax option
        },
        eventClick: function(info) {

            console.log(info);
            
            // info.el.style.borderColor = 'red';
        }

    });
    calendar.render();
    loadNotes();
});

</script>
@endsection
