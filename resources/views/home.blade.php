@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Events') }}</span>
                    @can('create-events')
                    <a href="{{ route('events.create') }}" class="btn btn-primary float-right btn-sm">Create Event</a>
                    @endcan
                </div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if( $events->count() )

                    <form action="{{ route('dashboard') }}" class="form-inline" method="GET">
                        <div class="form-group mb-2 mr-2">
                            <input type="text" name="title" placeholder="Event name..." class="form-control">
                        </div>
                        <div class="form-group mb-2 mr-2">
                            <select name="year" class="form-control">
                                <option value="" selected>Year</option>
                                <?php 
                                $y = date('Y')-10;
                                for ($x = $y; $x <= $y+20; $x++) { 
                                  echo '<option value="'.$x.'">'.$x.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group mb-2 mr-2">
                            <select name="month" class="form-control">
                                <option value="">Select Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
                    </form>

                    <table class="table table-inverse table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Guests</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                @can('create-events')
                                <th>Option</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($events as $event)
                            <tr>
                                <td><a href="{{ route('events.show',$event->id) }}">{{ $event->name }}</a></td>
                                <td>{{ $event->guests }}</td>
                                <td>{{ date('F j, Y, g:i a', strtotime($event->start_date)) }}</td>
                                <td>{{ date('F j, Y, g:i a', strtotime($event->end_date)) }}</td>
                                @can('create-events')
                                <td>
                                    <a href="{{ route('events.edit',$event->id) }}" class="btn btn-success btn-sm">Edit Event</a>
                                    <form action="{{ route('events.destroy',$event->id) }}" method="POST" style="float:right;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</a>
                                    </form>
                                </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr><br>
                    <div id="pager">
                        {{ $events->render(); }}
                    </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            There are no avaialble events to show at the moment.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<style>
#pager div:first-of-type{display: none;}
#pager svg{width: 25px;}
@media (max-width: 767px) {.fc .fc-toolbar-title{font-size: 1.3em;} #app {padding-top: 50px;}}
</style>
@endsection
