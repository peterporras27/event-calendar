@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Users') }}</span>
                    {{-- <a href="{{ route('users.create') }}" class="btn btn-primary float-right btn-sm">Create user</a> --}}
                </div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if( $users->count() )
                    <table class="table table-inverse table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Role</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                <td>
                                    @if($user->department)
                                        {{ $user->department->name }}
                                    @endif
                                </td>
                                <td>{{ ucwords($user->role) }}</td>
                                <td>
                                    <a href="{{ route('users.edit',$user->id) }}" class="btn btn-success btn-sm">Edit User</a>
                                    {{-- <form action="{{ route('users.destroy',$user->id) }}" method="POST" style="float:right;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</a>
                                    </form> --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr><br>
                    <div id="pager">
                        {{ $users->render(); }}
                    </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            There are no avaialble users to show at the moment.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<style>
#pager div:first-of-type{display: none;}
#pager svg{width: 25px;}
</style>
@endsection
