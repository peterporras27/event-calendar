@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Event Details') }}</span>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                          <tbody>
                            <tr>
                                <td><b>Name:</b></td>
                                <td>{{ $event->name }}</td>
                            </tr>
                            <tr>
                                <td><b>Start Date:</b></td>
                                <td>{{ date('F j, Y, g:i a', strtotime($event->start_date)) }}</td>
                            </tr>
                            <tr>
                                <td><b>End Date:</b></td>
                                <td>{{ date('F j, Y, g:i a', strtotime($event->end_date)) }}</td>
                            </tr>
                            <tr>
                                <td><b>Venue:</b></td>
                                <td>{{ $event->venue }}</td>
                            </tr>
                            <tr>
                                <td><b>Guests</b></td>
                                <td>{{ $event->guests }}</td>
                            </tr>
                            @if($event->departments()->count())
                            <tr>
                                <td><b>Department:</b></td>
                                <td>
                                    @foreach($event->departments() as $dep )
                                    {{$dep->name}} <br>
                                    @endforeach
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <td><b>Desctiption:</b></td>
                                <td>{{ $event->description }}</td>
                            </tr>

                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
