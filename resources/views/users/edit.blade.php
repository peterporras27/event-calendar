@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Edit User') }}</span>
                </div>

                <div class="card-body">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form action="{{ route('users.update',$user->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>First Name:</strong>
                                            <input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <strong>Last Name:</strong>
                                            <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>Middle Name:</strong>
                                            <input type="text" name="middle_name" value="{{ $user->middle_name }}" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <strong>Birthday:</strong>
                                            <input type="date" name="birthday" value="{{ $user->birthday }}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>Phone:</strong>
                                            <input type="number" name="phone" value="{{ $user->phone }}" class="form-control" placeholder="09" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>Email:</strong>
                                            <input type="text" name="email" value="{{ $user->email }}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    @if(Auth()->user()->role == 'admin')
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <strong>Role:</strong>
                                                <select name="role" class="form-control">
                                                    <option value="student"{{ $user->role == 'student' ? ' selected':'' }}>Student</option>
                                                    <option value="faculty"{{ $user->role == 'faculty' ? ' selected':'' }}>Faculty</option>
                                                    <option value="staff"{{ $user->role == 'staff' ? ' selected':'' }}>Staff</option>
                                                    <option value="admin"{{ $user->role == 'admin' ? ' selected':'' }}>Administrator</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <strong>Department:</strong><br>
                                            <select name="department_id" class="form-control">
                                            @foreach($departments as $department)
                                                <option value="{{ $department->id }}" {{ $department->id == $user->department_id ? ' selected':'' }}>{{ $department->name }}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary">Update Profile</button>
                            </div>
                        </div>
                    </form>  

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
