<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'venue',
        'guests'
    ];

    public function departments(){
        return Department::whereIn('id',json_decode($this->department_ids))->get();
    }
}
