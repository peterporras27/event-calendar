<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Event;
use Carbon\Carbon;
use Auth;

class EventController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role != 'admin') {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('events.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'start_date' => 'required|date',
            'start_time' => 'required',
            'end_date' => 'required|date',
            'end_time' => 'required',
        ]);
    
        $event = new Event;
        $event->department_ids = json_encode($request->input('department_ids'));
        $event->fill($request->all());
        $event->start_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date').' '.date('H:i:s', strtotime($request->input('start_time')) ) );
        $event->end_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date').' '.date('H:i:s', strtotime($request->input('end_time')) ) );

        $event->save();
    
        return redirect()->route('dashboard')
            ->with('success','Event successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $departments = Department::all();
        return view('events.show',compact('event','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $departments = Department::all();
        return view('events.edit',compact('event','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'start_date' => 'required|date',
            'start_time' => 'required',
            'end_date' => 'required|date',
            'end_time' => 'required',
        ]);
        
        $event->department_ids = json_encode($request->input('department_ids'));
        $event->fill($request->all());
        $event->start_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date').' '.date('H:i:s', strtotime($request->input('start_time')) ) );
        $event->end_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date').' '.date('H:i:s', strtotime($request->input('end_time')) ) );
        $event->save();
    
        return redirect()->route('events.edit',$event->id)
            ->with('success','Event successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return redirect('dashboard')->with('success','Event successfully removed.');
    }
}
