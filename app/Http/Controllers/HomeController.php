<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (Auth::user()->role != 'admin') {
            return redirect('/');
        }

        $hasFilter = false;

        if ($request->input('title')) {
            $hasFilter = true;
            $events = Event::where('name', 'like', '%'.$request->input('title').'%')
                ->orderBy('id','DESC')
                ->paginate(20);
        }

        if ($request->input('year')) {
            $hasFilter = true; 
            $events = Event::whereYear('start_date',$request->input('year'))
                ->orderBy('id','DESC')
                ->paginate(20);
        }

        if ($request->input('month')) {
            $hasFilter = true;
            $events = Event::whereMonth('start_date',$request->input('month'))
                ->orderBy('id','DESC')
                ->paginate(20);;
        }

        if (!$hasFilter) {

            $events = Event::orderBy('id','DESC')->paginate(20);
        }
        
        

        return view('home',compact('events'));
    }
}
