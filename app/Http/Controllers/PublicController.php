<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Event;
use App\Models\Device;

class PublicController extends Controller
{
    public function cron()
    {

        $from = date('Y-m-d');
        $in5 = date('Y-m-d', strtotime($from. ' + 5 days'));
        $in1 = date('Y-m-d', strtotime($from. ' + 1 days'));
        
        // $events = Event::whereBetween('start_date', [$from, $to])->get();

        $in5days = Event::whereDate('start_date', $in5)->get();
        $in1day = Event::whereDate('start_date', $in1)->get();
        $eventDay = Event::whereDate('start_date', $from)->get();

        $notifications = [];

        if ($in5days->count()) {
            foreach ($in5days as $five) {
                $notifications[] = array(
                    'title' => $five->name,
                    'message' => 'Event will begin in 5 days '.$in5,
                );
            }
        }

        if ($in1day->count()) {
            foreach ($in1day as $one) {
                $notifications[] = array(
                    'title' => $one->name,
                    'message' => 'Event will begin in tommorrow.',
                );
            }
        }

        if ($eventDay->count()) {
            foreach ($eventDay as $today) {
                $notifications[] = array(
                    'title' => $today->name,
                    'message' => 'Event will begin today!',
                );
            }
        }

        $devices = Device::all();

        $status = [];

        if ($devices) {
            foreach ($devices as $device) {
                foreach ($notifications as $notify => $notif) {

                    $status = $this->send_notification(
                        $device->device_id,
                        $notif['title'],
                        $notif['message']
                    );

                    if ($status->failure) {
                        $device->delete();
                    }
                }   
            }
        }

        return response()->json( $status );
    }

    public function event($id)
    {
        $event = Event::find($id);
        $departments = Department::all();
        return view('events.show',compact('event','departments'));
    }

    public function mobile(Request $request)
    {
        $results = null;

        if ($request->input('s')) {

            $results = Event::where('name','like', '%'.$request->input('s').'%')
                ->whereYear('start_date', '=', date('Y'))
                ->get();
        }

        if ($request->input('device')) {
            $device = Device::where('device_id','=', $request->input('device'))->first();
            if (!$device) {
                $dev = new Device;
                $dev->device_id = $request->input('device');
                $dev->save();
            }
        }

        return view('mobile', compact('results'));
    }

    public function dates(Request $request)
    {
        $month = date('m');

        if ($request->input('start')) {

        }

        if ($request->input('end')) {
            
        }


        if ($request->input('month')) {
            $month = date('m', strtotime($request->input('month')));
        }

        // $events = Event::whereMonth('start_date', '=', $month)->get();
        $events = Event::all();

        $return = [];

        if ($events->count()) 
        {
            foreach ($events as $event) {
                $return[] = [
                    'title' => $event->name,
                    'start' => $event->start_date,
                    'end' => $event->end_date,
                    'url' => route('event',$event->id),
                ];
            }
        }

        return response()->json( $return );
    }

    public function getNotifs()
    {

        $from = date('Y-m-d');
        $to = date('Y-m-d', strtotime($from. ' + 3 days'));

        $events = Event::whereBetween('start_date', [$from, $to])->get();


        return response()->json( $events );
    }

    public function send_notification($device_id,$title,$message) {

       $url = 'https://fcm.googleapis.com/fcm/send';
       $fields = array(
          'to' => $device_id,
          'priority' => 'high',
          'notification' => array(
             'title' => $title,
             'body' => $message,
             'icon' => 'https://wvsucalendar.xyz/images/calendar.png'
          )
       );
       
       // Firebase API Key
       $headers = array(
          'Authorization: key=AAAArOaZdJM:APA91bFAW2CrBQ5V05HWvxZTA5TyL-OZQw7qHdtI9ALagNJVFY3NMPhHpVWwU_pXl49VL16Xzjwnq0LKxMeSuEfDx56Xw7ZIYrH-u6jJic8eUHldUfEEsamf1b5oxYjUB0tuNUzgHqJd',
          'Content-Type: application/json'
       );
      // Open connection
       $ch = curl_init();
       // Set the url, number of POST vars, POST data
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);

       return json_decode($result);
    }
}







