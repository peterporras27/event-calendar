<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cron', [PublicController::class, 'cron'])->name('cron');
Route::get('/mobile', [PublicController::class, 'mobile'])->name('mobile');
Route::get('/dates', [PublicController::class, 'dates'])->name('dates');
Route::get('/notifs', [PublicController::class, 'getNotifs'])->name('notifs');
Route::get('/event/{id}', [PublicController::class, 'event'])->name('event');

require __DIR__.'/auth.php';

Auth::routes([
    // 'register' => false
]);

Route::get('/dashboard', [HomeController::class, 'index'])->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ['auth']], function() {

    // Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('events', EventController::class);
    Route::resource('departments', DepartmentController::class);
});