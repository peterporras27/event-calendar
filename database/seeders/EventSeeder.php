<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;
use App\Models\Event;
use App\Models\User;
use DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'student','faculty','staff','all'
        for ($i = 0; $i < 10; $i++) {

            $year = date('Y')+$i;
            $yearplus = $year+1;

            DB::table('events')->insert([
                [
                    'name' => 'FIRST DAY OF SERVICE (Faculty)',
                    'start_date' => $year.'-08-03 07:00:00',
                    'end_date' => $year.'-08-03 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FACULTY GENERAL ASSEMBLY FOR '.$year,
                    'start_date' => $year.'-08-03 07:00:00',
                    'end_date' => $year.'-08-03 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ON-LINE REGISTRATION / ENROLLMENT (ALL LEVELS)',
                    'start_date' => $year.'-08-10 07:00:00',
                    'end_date' => $year.'-08-20 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ISO 9001:2015 2nd Survaillance Audit (Tentative) - Grace Period will be Requested From SOCOTEC',
                    'start_date' => $year.'-08-13 07:00:00',
                    'end_date' => $year.'-08-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Instructional Materials Review for 1st Semester',
                    'start_date' => $year.'-08-15 07:00:00',
                    'end_date' => $year.'-08-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Enrollment of Life Long Learning Self Sufficient Program / LLSSPP (1st Semester)',
                    'start_date' => $year.'-08-17 07:00:00',
                    'end_date' => $year.'-08-18 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Scholarship Orientation (COM)',
                    'start_date' => $year.'-08-19 07:00:00',
                    'end_date' => $year.'-08-19 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Ninoy Aquino (Special Non Working Holiday)',
                    'start_date' => $year.'-08-21 07:00:00',
                    'end_date' => $year.'-08-21 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'First Day of Classes (LLSSPP)',
                    'start_date' => $year.'-08-29 07:00:00',
                    'end_date' => $year.'-08-29 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'National Heroes Day (Regular Holiday)',
                    'start_date' => $year.'-08-31 07:00:00',
                    'end_date' => $year.'-08-31 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST DAY OF CLASSES (COM)',
                    'start_date' => $year.'-09-07 07:00:00',
                    'end_date' => $year.'-09-07 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'University Freshmen Orientation',
                    'start_date' => $year.'-09-11 07:00:00',
                    'end_date' => $year.'-09-11 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST DAY OF CLASSES (ILS,UG)',
                    'start_date' => $year.'-09-14 07:00:00',
                    'end_date' => $year.'-09-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Changing, Adding and Dropping Period (UG, COM & SG)',
                    'start_date' => $year.'-09-14 07:00:00',
                    'end_date' => $year.'-09-19 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => '23rd Extension In - House Review',
                    'start_date' => $year.'-09-16 07:00:00',
                    'end_date' => $year.'-09-18 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST DAY OF CLASSES (SG)',
                    'start_date' => $year.'-09-19 07:00:00',
                    'end_date' => $year.'-09-19 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SLDP',
                    'start_date' => $year.'-10-02 07:00:00',
                    'end_date' => $year.'-10-02 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Teachers\' Day',
                    'start_date' => $year.'-10-05 07:00:00',
                    'end_date' => $year.'-10-05 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Deadline for Submission of Faculty CHED E-0 for 1st Semester',
                    'start_date' => $year.'-10-14 07:00:00',
                    'end_date' => $year.'-10-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'University Corriculum Review',
                    'start_date' => $year.'-10-30 07:00:00',
                    'end_date' => $year.'-10-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'All Saint\'s Day (Special Non Working Holiday)',
                    'start_date' => $year.'-11-01 07:00:00',
                    'end_date' => $year.'-11-01 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'All Soul\'s Day (Special Non Working Holiday)',
                    'start_date' => $year.'-11-02 07:00:00',
                    'end_date' => $year.'-11-02 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'MIDTERM EXAMINATION (ILS-SHS, UG)',
                    'start_date' => $year.'-11-10 07:00:00',
                    'end_date' => $year.'-11-13 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => '37th Research In-house Review',
                    'start_date' => $year.'-11-12 07:00:00',
                    'end_date' => $year.'-11-13 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'MIDTERM EXAMINATIONS (GS)',
                    'start_date' => $year.'-11-14 07:00:00',
                    'end_date' => $year.'-11-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST PERIODICAL EXAMINATIONS (ILS)',
                    'start_date' => $year.'-11-17 07:00:00',
                    'end_date' => $year.'-11-20 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Bonifacio Day (Regular Holiday)',
                    'start_date' => $year.'-11-30 07:00:00',
                    'end_date' => $year.'-11-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Institutional Accreditation Visit (Tentative)',
                    'start_date' => $year.'-12-01 07:00:00',
                    'end_date' => $year.'-12-10 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Feast of Immaculate Concepcion (Special Non-working Holiday)',
                    'start_date' => $year.'-12-08 07:00:00',
                    'end_date' => $year.'-12-08 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Graciano Lopez-Jaena Day (Local Holiday)',
                    'start_date' => $year.'-12-18 07:00:00',
                    'end_date' => $year.'-12-18 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'System-wide WVSU - College Admition Test (Tentative)',
                    'start_date' => $year.'-12-20 07:00:00',
                    'end_date' => $year.'-12-20 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Christmas Break',
                    'start_date' => $year.'-12-20 07:00:00',
                    'end_date' => $yearplus.'-01-03 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'CLASSES RESUME',
                    'start_date' => $yearplus.'-01-04 07:00:00',
                    'end_date' => $yearplus.'-01-04 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Enrollment of LLSSP (2nd Semester)',
                    'start_date' => $yearplus.'-01-04 07:00:00',
                    'end_date' => $yearplus.'-01-05 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'PE Day',
                    'start_date' => $yearplus.'-01-08 07:00:00',
                    'end_date' => $yearplus.'-01-08 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'First Day of Classes (LLSSP)',
                    'start_date' => $yearplus.'-01-16 07:00:00',
                    'end_date' => $yearplus.'-01-16 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SEMESTRAL BREAK (COM)',
                    'start_date' => $yearplus.'-01-25 07:00:00',
                    'end_date' => $yearplus.'-02-09 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'UNIVERSITY DAY',
                    'start_date' => $yearplus.'-01-27 07:00:00',
                    'end_date' => $yearplus.'-01-27 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS (GS)',
                    'start_date' => $yearplus.'-01-30 07:00:00',
                    'end_date' => $yearplus.'-01-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS (ILS-SHS, UG)',
                    'start_date' => $yearplus.'-02-01 07:00:00',
                    'end_date' => $yearplus.'-02-05 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SEMESTRAL BREAK (GS)',
                    'start_date' => $yearplus.'-02-01 07:00:00',
                    'end_date' => $yearplus.'-02-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SEMESTRAL BREAK (ILS-SHS, UG)',
                    'start_date' => $yearplus.'-02-06 07:00:00',
                    'end_date' => $yearplus.'-02-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SECOND PERIODICAL EXAMINATIONS (ILS)',
                    'start_date' => $yearplus.'-02-08 07:00:00',
                    'end_date' => $yearplus.'-02-10 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SEMESTRAL BREAK (ILS)',
                    'start_date' => $yearplus.'-02-11 07:00:00',
                    'end_date' => $yearplus.'-02-21 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'LAST DAY OF SUBMISSION OF FINAL GRADES (1st SEM, AY '.$year.' - '.$yearplus.')',
                    'start_date' => $yearplus.'-02-23 07:00:00',
                    'end_date' => $yearplus.'-02-23 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Jaro Fiesta',
                    'start_date' => $yearplus.'-02-02 07:00:00',
                    'end_date' => $yearplus.'-02-02 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ON-LINE REGISTRATION / ENROLLMENT (ILS-SHS, UG, GS)',
                    'start_date' => $yearplus.'-02-08 07:00:00',
                    'end_date' => $yearplus.'-02-20 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ON-LINE REGISTRATION / ENROLLMENT (COM)',
                    'start_date' => $yearplus.'-02-10 07:00:00',
                    'end_date' => $yearplus.'-02-12 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Instructional Materials Review for 2nd Semester',
                    'start_date' => $yearplus.'-02-10 07:00:00',
                    'end_date' => $yearplus.'-02-20 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Evilio Javier Day (Local Holiday)',
                    'start_date' => $yearplus.'-02-11 07:00:00',
                    'end_date' => $yearplus.'-02-11 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Chinese New Year (Special Non-working Holiday)',
                    'start_date' => $yearplus.'-02-12 07:00:00',
                    'end_date' => $yearplus.'-02-12 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST DAY OF CLASSES (COM)',
                    'start_date' => $yearplus.'-02-15 07:00:00',
                    'end_date' => $yearplus.'-02-15 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Changing, Adding, and Dropping Period (COM)',
                    'start_date' => $yearplus.'-02-15 07:00:00',
                    'end_date' => $yearplus.'-02-16 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST DAY OF CLASSES (ILS-SHS, UG)',
                    'start_date' => $yearplus.'-02-22 07:00:00',
                    'end_date' => $yearplus.'-02-22 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'CLASSES RESUME FOR ILS',
                    'start_date' => $yearplus.'-02-22 07:00:00',
                    'end_date' => $yearplus.'-02-22 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Changing, Adding, and Dropping Period (UG, GS)',
                    'start_date' => $yearplus.'-02-22 07:00:00',
                    'end_date' => $yearplus.'-02-27 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'People Power Day (Special Non-working Holiday)',
                    'start_date' => $yearplus.'-02-25 07:00:00',
                    'end_date' => $yearplus.'-02-25 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'SLDP',
                    'start_date' => $yearplus.'-02-26 07:00:00',
                    'end_date' => $yearplus.'-02-26 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIST DAY OF CLASSES (GS)',
                    'start_date' => $yearplus.'-02-27 07:00:00',
                    'end_date' => $yearplus.'-02-27 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'University Pag-iririmaw',
                    'start_date' => $yearplus.'-03-05 07:00:00',
                    'end_date' => $yearplus.'-03-05 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Advocacy Week',
                    'start_date' => $yearplus.'-03-15 07:00:00',
                    'end_date' => $yearplus.'-03-19 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Libaration of Panay (Local Holiday)',
                    'start_date' => $yearplus.'-03-18 07:00:00',
                    'end_date' => $yearplus.'-03-18 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Deadline of Submission of Faculty CHED E-0 for 2nd Semester',
                    'start_date' => $yearplus.'-03-22 07:00:00',
                    'end_date' => $yearplus.'-03-22 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Maundy Thursday (Regular Holiday)',
                    'start_date' => $yearplus.'-04-01 07:00:00',
                    'end_date' => $yearplus.'-04-01 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Good Friday (Regular Holiday)',
                    'start_date' => $yearplus.'-04-02 07:00:00',
                    'end_date' => $yearplus.'-04-02 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'University Corriculum Review',
                    'start_date' => $yearplus.'-04-07 07:00:00',
                    'end_date' => $yearplus.'-04-07 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Araw ng Kagitingan (Regular Holiday)',
                    'start_date' => $yearplus.'-04-09 07:00:00',
                    'end_date' => $yearplus.'-04-09 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'THIRD PERIODICAL EXAMINATIONS (ILS)',
                    'start_date' => $yearplus.'-04-19 07:00:00',
                    'end_date' => $yearplus.'-04-21 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'MIDTERM EXAMINATIONS (ILS-SHS, UG)',
                    'start_date' => $yearplus.'-04-20 07:00:00',
                    'end_date' => $yearplus.'-04-23 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => '38th Research In-House Review',
                    'start_date' => $yearplus.'-04-20 07:00:00',
                    'end_date' => $yearplus.'-04-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'International Labor Day (Regular Holiday)',
                    'start_date' => $yearplus.'-05-01 07:00:00',
                    'end_date' => $yearplus.'-05-01 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'MIDTERM EXAMNIATIONS (GS) - Make-up Classes in lieu of May 1, '.$yearplus,
                    'start_date' => $yearplus.'-05-02 07:00:00',
                    'end_date' => $yearplus.'-05-02 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Eidul - Fitar (Regular Holiday)',
                    'start_date' => $yearplus.'-05-13 07:00:00',
                    'end_date' => $yearplus.'-05-13 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'RealITy Day (CICT)',
                    'start_date' => $yearplus.'-05-14 07:00:00',
                    'end_date' => $yearplus.'-05-14 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Enrollment of LLSSP (Summer)',
                    'start_date' => $yearplus.'-05-20 07:00:00',
                    'end_date' => $yearplus.'-05-21 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'University Kaathagan (2nd Semester)',
                    'start_date' => $yearplus.'-05-28 07:00:00',
                    'end_date' => $yearplus.'-05-28 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Intermidiate Competencies Assesment',
                    'start_date' => $yearplus.'-06-02 07:00:00',
                    'end_date' => $yearplus.'-06-18 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'First Day of Classes (LLSSP)',
                    'start_date' => $yearplus.'-06-05 07:00:00',
                    'end_date' => $yearplus.'-06-05 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'WVSU Foundation Day',
                    'start_date' => $yearplus.'-06-06 07:00:00',
                    'end_date' => $yearplus.'-06-06 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Search for Most Outstanding College Council, Student Organization, and Publication',
                    'start_date' => $yearplus.'-06-07 07:00:00',
                    'end_date' => $yearplus.'-06-07 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ACADEMIC COUNCIL MEETING',
                    'start_date' => $yearplus.'-06-11 07:00:00',
                    'end_date' => $yearplus.'-06-11 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'PE Day',
                    'start_date' => $yearplus.'-06-11 07:00:00',
                    'end_date' => $yearplus.'-06-11 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Independence Day (Regular Holiday)',
                    'start_date' => $yearplus.'-06-12 07:00:00',
                    'end_date' => $yearplus.'-06-12 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Cultural Day',
                    'start_date' => $yearplus.'-06-18 07:00:00',
                    'end_date' => $yearplus.'-06-18 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS (ILS Graduating Students)',
                    'start_date' => $yearplus.'-06-28 07:00:00',
                    'end_date' => $yearplus.'-06-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS (ILS-SHS, UG)',
                    'start_date' => $yearplus.'-06-29 07:00:00',
                    'end_date' => $yearplus.'-07-02 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS (GS)',
                    'start_date' => $yearplus.'-07-03 07:00:00',
                    'end_date' => $yearplus.'-07-03 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS (ILS Lower Years & SHS G-11)',
                    'start_date' => $yearplus.'-07-05 07:00:00',
                    'end_date' => $yearplus.'-07-07 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'College /  Campus Recognition Programs',
                    'start_date' => $yearplus.'-07-06 07:00:00',
                    'end_date' => $yearplus.'-07-09 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Kinder Garten Moving Up',
                    'start_date' => $yearplus.'-07-08 07:00:00',
                    'end_date' => $yearplus.'-07-08 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'LAST DAY OF SUBMISSION OF FINAL GRADES (2nd SEM, AY '.$year.' - '.$yearplus.')',
                    'start_date' => $yearplus.'-07-16 07:00:00',
                    'end_date' => $yearplus.'-07-16 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Medical Exam for Incoming Clerks',
                    'start_date' => $yearplus.'-06-21 07:00:00',
                    'end_date' => $yearplus.'-06-22 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ENROLLMENT OF CLINICAL CLERKS (COM)',
                    'start_date' => $yearplus.'-06-25 07:00:00',
                    'end_date' => $yearplus.'-06-26 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'COM College and Hospital Orientation of Clerks',
                    'start_date' => $yearplus.'-06-27 07:00:00',
                    'end_date' => $yearplus.'-06-29 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Start of Clinical Clerkship (COM)',
                    'start_date' => $yearplus.'-07-01 07:00:00',
                    'end_date' => $yearplus.'-07-01 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'ENROLLMENT PERIOD',
                    'start_date' => $yearplus.'-07-12 07:00:00',
                    'end_date' => $yearplus.'-07-16 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FIRST DAY OF CLASSES',
                    'start_date' => $yearplus.'-07-19 07:00:00',
                    'end_date' => $yearplus.'-07-19 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'MIDTERM EXAMINATIONS',
                    'start_date' => $yearplus.'-07-29 07:00:00',
                    'end_date' => $yearplus.'-07-29 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Deadline of Submission of Faculty CHED E-0 for Summer '.$yearplus,
                    'start_date' => $yearplus.'-07-29 07:00:00',
                    'end_date' => $yearplus.'-07-29 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'Graduation (COM)',
                    'start_date' => $yearplus.'-07-30 07:00:00',
                    'end_date' => $yearplus.'-07-30 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => 'FINAL EXAMINATIONS',
                    'start_date' => $yearplus.'-08-12 07:00:00',
                    'end_date' => $yearplus.'-08-12 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ],[
                    'name' => '39th Research In-House Review',
                    'start_date' => $yearplus.'-08-26 07:00:00',
                    'end_date' => $yearplus.'-08-27 17:00:00',
                    'guests' => 'all',
                    'department_ids' => '["1","2","3","4"]',
                ]
            ]);
        }
    }
}
