<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Department;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $permissions = [
            
            'create-users', // 1 
            'read-users', // 2
            'update-users', // 3
            'delete-users', // 4
            'edit-users', // 5

            'create-roles', // 6
            'read-roles', // 7
            'update-roles', // 8
            'delete-roles', // 9
            'edit-roles', // 10

            'create-permissions', // 11
            'read-permissions', // 12
            'update-permissions', // 13
            'delete-permissions', // 14
            'edit-permissions', // 15

            'create-events', // 16
            'read-events', // 17
            'update-events', // 18
            'delete-events', // 19
            'edit-events', // 20
        ];
     
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        $student = Role::create(['name' => 'student']);
        $sudentPerm = Permission::where('name', '=', 'read-events')->get();
        $student->syncPermissions($sudentPerm);

        $faculty = Role::create(['name' => 'faculty']);
        $facultyPerm = Permission::where('name', '=', 'read-events')->get();
        $faculty->syncPermissions($facultyPerm);

        $staff = Role::create(['name' => 'staff']);
        $staffPerm = Permission::where('name', '=', 'read-events')->get();
        $staff->syncPermissions($staffPerm);

        $user = User::create([
            'first_name' => 'Admintrator',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
            'role' => 'admin'
        ]);
    
        $role = Role::create(['name' => 'admin']);
        $adminPerm = Permission::all();
        $role->syncPermissions($adminPerm);
        $user->assignRole([$role->id]);

        $coict = Department::create(['name' => 'College of Information and Communications Technology']);
        $coe = Department::create(['name' => 'College of Education']);
        $cobm = Department::create(['name' => 'College of Business and Management']);
        $soit = Department::create(['name' => 'College of Industrial Technology']);

        $this->call(EventSeeder::class);
    }
}
